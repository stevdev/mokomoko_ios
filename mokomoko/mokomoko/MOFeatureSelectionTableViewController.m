//
//  MOFeatureSelectionTableViewController.m
//  mokomoko
//
//  Created by steffen on 07/04/14.
//  Copyright (c) 2014 mokomoko. All rights reserved.
//

#import "MOFeatureSelectionTableViewController.h"
#import "BRFlabbyTableManager.h"
#import "BRFlabbyTableViewCell.h"
#import "JSObjection.h"

#pragma mark - private interface

typedef NS_ENUM(NSUInteger, MOFeatureCellType) {
    MOFeatureCellType_Party,
    MOFeatureCellType_Chill,
    MOFeatureCellType_Play
};

@interface MOFeatureSelectionTableViewController () <BRFlabbyTableManagerDelegate>

@property (strong) BRFlabbyTableManager *flabbyTableManager;

@end

#pragma mark - implementation

@implementation MOFeatureSelectionTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView setContentInset:UIEdgeInsetsMake(50,0,0,0)];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.tableView registerClass:[BRFlabbyTableViewCell class]
           forCellReuseIdentifier:NSStringFromClass([BRFlabbyTableViewCell class])];
    
    self.flabbyTableManager = [[BRFlabbyTableManager alloc] initWithTableView:self.tableView];
    [self.flabbyTableManager setDelegate:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

#pragma mark - Table view delegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    id vc = nil;
    
    switch (indexPath.row) {
            
//        case MOFeatureCellType_Party:
//            
//            break;
//            
//        case MOFeatureCellType_Chill:
//            
//            break;
//            
//        case MOFeatureCellType_Play:
//            
//            break;
            
        default:
            vc = [[JSObjection defaultInjector] getObject:@"PlayVC"];
            break;
    }
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - BRFlabbyTableManagerDelegate

- (UIColor *)flabbyTableManager:(BRFlabbyTableManager *)tableManager flabbyColorForIndexPath:(NSIndexPath *)indexPath{
    
    return [self _colorForCellAtIndexPath:indexPath];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BRFlabbyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([BRFlabbyTableViewCell class])
                                                                  forIndexPath:indexPath];
    [cell setFlabby:YES];
    [cell setLongPressAnimated:YES];
    [cell setFlabbyColor:[self _colorForCellAtIndexPath:indexPath]];
    cell.textLabel.font = [UIFont fontWithName:@"Forte-Line" size:24];
    cell.textLabel.text = [self _textForCellAtIndexPath:indexPath];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 150.0;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 50.0;
//}

#pragma mark - private

- (UIColor *)_colorForCellAtIndexPath:(NSIndexPath *)indexPath {
    
    UIColor *color = nil;
    
    switch (indexPath.row) {
            
        case MOFeatureCellType_Party:
            color = [UIColor lightGrayColor];
            break;
            
        case MOFeatureCellType_Chill:
            color = [UIColor grayColor];
            break;
            
        case MOFeatureCellType_Play:
            color = [UIColor orangeColor];
            break;
            
        default:
            color = [UIColor darkGrayColor];
            break;
    }
    
    return color;
}

- (NSString *)_textForCellAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *text = nil;
    
    switch (indexPath.row) {
            
        case MOFeatureCellType_Party:
            text = @"Let's party!";
            break;
            
        case MOFeatureCellType_Chill:
            text = @"Let's chill!";
            break;
            
        case MOFeatureCellType_Play:
            text = @"Let's play!";
            break;
            
        default:
            text = @"Let's party!";
            break;
    }
    
    return text;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

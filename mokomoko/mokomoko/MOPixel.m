//
//  DUBPixel.m
//  OSCControl
//
//  Created by steven reinisch on 9/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MOPixel.h"

@implementation MOPixel

@synthesize red, green, blue;

- (id)init {
    self = [super init];
    
    if (self) {
        red     = 0;
        green   = 0;
        blue    = 0;
    }
    
    return self;
}

@end

//
//  main.m
//  mokomoko
//
//  Created by steffen on 02/04/14.
//  Copyright (c) 2014 mokomoko. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MOAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MOAppDelegate class]));
    }
}

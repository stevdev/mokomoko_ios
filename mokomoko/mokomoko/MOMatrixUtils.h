//
//  DUBMatrixUtils.h
//  OSCControl
//
//  Created by steven reinisch on 9/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MOPixel;

#define RED_PIXEL_AT(row, led) _pxl = [[self.matrix objectAtIndex:row] objectAtIndex:led];\
                               _pxl.red   = 31;\
                               _pxl.green = 0;\
                               _pxl.blue  = 0;

#define GREEN_PIXEL_AT(row, led) _pxl = [[self.matrix objectAtIndex:row] objectAtIndex:led];\
                                 _pxl.red   = 0;\
                                 _pxl.green = 31;\
                                 _pxl.blue  = 0;

#define BLUE_PIXEL_AT(row, led) _pxl = [[self.matrix objectAtIndex:row] objectAtIndex:led];\
                                _pxl.red   = 0;\
                                _pxl.green = 0;\
                                _pxl.blue  = 31;

#define BLACK_PIXEL_AT(row, led) _pxl = [[self.matrix objectAtIndex:row] objectAtIndex:led];\
                                 _pxl.blue = 0;\
                                 _pxl.green = 0;\
                                 _pxl.red = 0;

@interface MOMatrixUtils : NSObject

+ (NSArray*)matrix;

+ (NSArray*)matrixWithPixelInitBlock:(void (^) (MOPixel *pxl, uint8_t row, uint8_t led))initBlock;

+ (NSArray*)matrixWithRed:(uint8_t)r
                    green:(uint8_t)g
                     blue:(uint8_t)b;

+ (NSArray*)copyMatrix:(NSArray*)toCopy;

+ (NSArray*)serializeMatrix:(NSArray*)matrix;

+ (void)clearMatrix:(NSArray*)matrix;

@end

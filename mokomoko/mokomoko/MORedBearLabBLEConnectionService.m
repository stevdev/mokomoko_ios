//
//  DUBRedBearLabBLEMatrixSender.m
//  MatrixEQ
//
//  Created by steffen on 28/03/14.
//  Copyright (c) 2014 dubmas. All rights reserved.
//

#import "MORedBearLabBLEConnectionService.h"
#import "MOMatrixUtils.h"
#import "MOPixel.h"
#import "BLE.h"

#pragma mark - private interface

//#define CHUNK_SIZE 18
#define CHUNK_SIZE 20

@interface MORedBearLabBLEConnectionService () <BLEDelegate>

@property (atomic, assign) BOOL sending;
@property (assign) dispatch_source_t sendTimer;
@property (assign) NSUInteger nextChunkToSend;
@property (copy) NSArray *chunksToSend;

@property (strong) BLE *bleShield;

@property (copy) void (^startCompletion) (NSError *);

@end

#pragma mark - implementation

@implementation MORedBearLabBLEConnectionService

- (void)connectWithCompletion:(void (^)(NSError *))completion {
    
    self.startCompletion = completion;
    
    self.bleShield = [[BLE alloc] init];
    [self.bleShield controlSetup];
    self.bleShield.delegate = self;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        NSInteger scanResult = [self.bleShield findBLEPeripherals:10];
        
        if (scanResult == 0) {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            //dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 4 * NSEC_PER_SEC), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                if(self.bleShield.peripherals.count > 0) {
                    [self.bleShield connectPeripheral:[self.bleShield.peripherals objectAtIndex:0]];
                }
            });
        }
    });    
}

- (void)sendMatrix:(NSArray*)matrix {
    
    if (!self.sending) {
        
        self.sending = YES;
        
        NSData *matrixData = [self _matrixToNSData:matrix];
        //NSData *matrixData = [self _matrixToNSDataWithPixelNumbers:matrix];
        self.chunksToSend  = [self _splitData:matrixData intoChunksWithSize:CHUNK_SIZE];
        
        dispatch_resume(self.sendTimer);
        
    } else {
        
        NSLog(@"still sending");
    }
}

#pragma mark - BLEDelegate

-(void) bleDidConnect {
    
    //1 ms : 1000000L, 1s : 1000000000L
    self.sendTimer = [self _createDispatchTimerWithInterval:10000L executeBlock:^{
        
        if (self.nextChunkToSend < self.chunksToSend.count) {
            
            [self.bleShield write:[self.chunksToSend objectAtIndex:self.nextChunkToSend++]];
            
        } else {
            
            dispatch_suspend(self.sendTimer);
            self.nextChunkToSend = 0;
            self.chunksToSend  = nil;
            self.sending       = NO;
        }
    }];
    
    self.startCompletion(nil);
}

#pragma mark - private

- (NSData *)_matrixToNSData:(NSArray*)matrix {
    
    NSArray *serializedMatrix   = [MOMatrixUtils serializeMatrix:matrix];
    NSUInteger byteStreamSize   = serializedMatrix.count * 3;//3 bytes per pixel
    
    uint8_t byteArray[byteStreamSize];
    memset(&byteArray, 0, byteStreamSize);
    
    for (NSUInteger pixelNr = 0; pixelNr < serializedMatrix.count; pixelNr++) {
        
        MOPixel *p = [serializedMatrix objectAtIndex:pixelNr];
        
        byteArray[(pixelNr * 3)]        = p.red;
        byteArray[(pixelNr * 3) + 1]    = p.green;
        byteArray[(pixelNr * 3) + 2]    = p.blue;
    }
    
    NSData *data = [NSData dataWithBytes:&byteArray length:byteStreamSize];
    
    return data;
}

- (NSData *)_matrixToNSDataWithPixelNumbers:(NSArray*)matrix {
    
    NSArray *serializedMatrix   = [MOMatrixUtils serializeMatrix:matrix];
    NSUInteger byteStreamSize   = serializedMatrix.count * 4;//3 bytes per pixelt + 1 for pixelNr
    
    uint8_t byteArray[byteStreamSize];
    memset(&byteArray, 0, byteStreamSize);
    
    for (NSUInteger pixelNr = 0; pixelNr < serializedMatrix.count; pixelNr++) {
        
        MOPixel *p = [serializedMatrix objectAtIndex:pixelNr];
        
        byteArray[(pixelNr * 4)]        = pixelNr;
        byteArray[(pixelNr * 4) + 1]    = p.red;
        byteArray[(pixelNr * 4) + 2]    = p.green;
        byteArray[(pixelNr * 4) + 3]    = p.blue;
    }

    NSData *data = [NSData dataWithBytes:&byteArray length:byteStreamSize];
    
    return data;
}

- (NSArray *)_splitData:(NSData *)data intoChunksWithSize:(NSUInteger)chunkSize {
    
    NSMutableArray *chunks  = [NSMutableArray array];
    NSUInteger offset       = 0;
    
    do {
        NSUInteger thisChunkSize = data.length - offset > chunkSize ? chunkSize : data.length - offset;
        
        NSData *chunk = [NSData dataWithBytes:(char *)[data bytes] + offset
                                       length:thisChunkSize];
        offset += thisChunkSize;
        [chunks addObject:chunk];
        
    } while (offset < data.length);
    
    return chunks;
}

- (dispatch_source_t) _createDispatchTimerWithInterval:(uint64_t) interval
                                          executeBlock:(dispatch_block_t) block {
    
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER,
                                                     0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
    if (timer) {
        dispatch_source_set_timer(timer, dispatch_time(DISPATCH_TIME_NOW, 0), interval, 0);
        dispatch_source_set_event_handler(timer, block);
    }
    
    return timer;
}

@end

//
//  MOPlayViewController.m
//  mokomoko
//
//  Created by steffen on 22/04/14.
//  Copyright (c) 2014 mokomoko. All rights reserved.
//

#import "MOPlayViewController.h"
#import "Objection.h"
#import "P_MOMatrixController.h"
#import "P_MOMokoMokoConnectionService.h"
#import "MOMatrixUtils.h"
#import "MOPixel.h"

#pragma mark - private interface

@interface MOPlayViewController ()

@property (strong) id<P_MOMatrixController> matrixController;
@property (strong) id<P_MOMokoMokoConnectionService> connectionService;

@end

#pragma mark - implementation

@implementation MOPlayViewController

objection_requires(@"matrixController", @"connectionService")

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:@"MOPlayViewController" bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"MyCell"];

//    UICollectionViewFlowLayout *myLayout = [[UICollectionViewFlowLayout alloc]init];
//    [myLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
//    [self.collectionView setCollectionViewLayout:myLayout];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 49;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MyCell"
                                                                           forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor lightGrayColor];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSIndexPath *matrixIndexPath    = [self.matrixController matrixIndexFromPixelWithNr:indexPath.row];
    NSUInteger pxlRow               = [matrixIndexPath indexAtPosition:0];
    NSUInteger pxlCol               = [matrixIndexPath indexAtPosition:1];
    
    NSArray *matrix = [MOMatrixUtils matrixWithPixelInitBlock:^(MOPixel *pxl, uint8_t row, uint8_t led) {
        
        if (row == pxlRow && pxlCol == led) {
            
            pxl.red     = 0;
            pxl.green   = 0;
            pxl.blue    = 254;
        }
    }];
    
    [self.connectionService sendMatrix:matrix];
}

#pragma mark Collection view layout things
// Layout: Set cell size
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    

    return CGSizeMake(40, 40);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 4.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    return 5.0;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    return UIEdgeInsetsMake(70,6,2,6);  // top, left, bottom, right
}


@end

//
//  DUBPixel.h
//  OSCControl
//
//  Created by steven reinisch on 9/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MOPixel : NSObject

@property (assign) int8_t red;
@property (assign) int8_t green;
@property (assign) int8_t blue;

@end

//
//  P_MOMatrixController.h
//  mokomoko
//
//  Created by steffen on 23/04/14.
//  Copyright (c) 2014 mokomoko. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol P_MOMatrixController <NSObject>

- (NSUInteger) nrOfRows;
- (NSUInteger) nrOfColumns;

- (NSIndexPath *) matrixIndexFromPixelWithNr:(NSUInteger)pixelNr;

@end

//
//  P_MOMokoMokoConnectionService.h
//  mokomoko
//
//  Created by steffen on 07/04/14.
//  Copyright (c) 2014 mokomoko. All rights reserved.
//

extern NSString *const MOMokoMokoConnectionServiceDidConnectNotification;
extern NSString *const MOMokoMokoConnectionServiceDidDisconnectConnectNotification;

@protocol P_MOMokoMokoConnectionService <NSObject>

- (void)connectWithCompletion:(void (^)(NSError *))completion;

/*
 * matrix: two dimensional array of DUBPixel
 */
- (void)sendMatrix:(NSArray*)matrix;

@end

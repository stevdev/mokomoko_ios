//
//  DUBMatrixUtils.m
//  OSCControl
//
//  Created by steven reinisch on 9/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MOMatrixUtils.h"
#import "MOMatrixConstants.h"
#import "MOPixel.h"

@interface MOMatrixUtils ()

@end

#pragma mark -

@implementation MOMatrixUtils

+ (NSArray*)matrix {
    return [MOMatrixUtils matrixWithPixelInitBlock:nil];
}

+ (NSArray*)matrixWithRed:(uint8_t)r
                    green:(uint8_t)g
                     blue:(uint8_t)b {
    
    return [MOMatrixUtils matrixWithPixelInitBlock:^(MOPixel *pxl, uint8_t row, uint8_t led) {
        pxl.red     = r;
        pxl.green   = g;
        pxl.blue    = b;
    }];
}

+ (NSArray*)copyMatrix:(NSArray*)toCopy {
    
    NSArray *matrix = [MOMatrixUtils matrix];
    
    for (uint8_t row = 0; row < ROWS; ++row) {
        
        for (uint8_t led = 0; led < LEDS_PER_ROW; ++led) {
            
            [[matrix objectAtIndex:row] 
                insertObject:[[toCopy objectAtIndex:row] objectAtIndex:led] 
                     atIndex:led];
        }
    }
    
    return matrix;
}

+ (NSArray*)serializeMatrix:(NSArray*)matrix {
    
    NSMutableArray *serializedMatrix = [NSMutableArray arrayWithCapacity:ROWS * LEDS_PER_ROW];
    
    for (uint8_t row = 0; row < ROWS; ++row) {
        
        for (uint8_t led = 0; led < LEDS_PER_ROW; ++led) {
            
            [serializedMatrix insertObject:[[matrix objectAtIndex:row] objectAtIndex:led] 
                                   atIndex:((row * LEDS_PER_ROW) + led)];

        }
    }
    
    return serializedMatrix;
}

#pragma mark - private

+ (NSArray*)matrixWithPixelInitBlock:(void (^) (MOPixel *pxl, uint8_t row, uint8_t led))initBlock {
    
    NSMutableArray *matrix = [NSMutableArray arrayWithCapacity:ROWS];
    
    for (uint8_t row = 0; row < ROWS; ++row) {
        
        NSMutableArray *aRow = [NSMutableArray arrayWithCapacity:LEDS_PER_ROW];
        
        for (uint8_t led = 0; led < LEDS_PER_ROW; ++led) {

            MOPixel *pxl = [[MOPixel alloc] init];
            
            if (initBlock) {
                initBlock(pxl, row, led);
            }
            
            [aRow insertObject:pxl atIndex:led];
        }
        
        [matrix insertObject:aRow atIndex:row];
    }
    
    return matrix;
}

+ (void)clearMatrix:(NSArray*)matrix {
    MOPixel *pxl = nil;
    for (uint8_t row = 0; row < ROWS; ++row) {
        for (uint8_t led = 0; led < LEDS_PER_ROW; ++led) {
            pxl       = [[matrix objectAtIndex:row] objectAtIndex:led];
            pxl.red   = 0;
            pxl.green = 0;
            pxl.blue  = 0;
        }
    }
}

@end

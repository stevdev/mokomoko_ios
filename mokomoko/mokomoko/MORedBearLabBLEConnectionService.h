//
//  DUBRedBearLabBLEMatrixSender.h
//  MatrixEQ
//
//  Created by steffen on 28/03/14.
//  Copyright (c) 2014 dubmas. All rights reserved.
//

#import "P_MOMokoMokoConnectionService.h"

@interface MORedBearLabBLEConnectionService : NSObject <P_MOMokoMokoConnectionService>

@end

//
//  MOAppDelegate.h
//  mokomoko
//
//  Created by steffen on 02/04/14.
//  Copyright (c) 2014 mokomoko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MOAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

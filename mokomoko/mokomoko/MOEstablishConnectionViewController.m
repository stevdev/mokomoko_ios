//
//  MOEstablishConnectionViewController.m
//  mokomoko
//
//  Created by steffen on 02/04/14.
//  Copyright (c) 2014 mokomoko. All rights reserved.
//

#import "MOEstablishConnectionViewController.h"
#import "JSObjection.h"
#import "Objection.h"
#import "P_MOMokoMokoConnectionService.h"
#import "MOAppDelegate.h"

@interface MOEstablishConnectionViewController ()

@property (strong) id<P_MOMokoMokoConnectionService> connectionService;

@property (strong, nonatomic) IBOutlet UILabel *gettingJiggyLabel;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *connectionActivityIndicator;


@end

@implementation MOEstablishConnectionViewController

objection_requires(@"connectionService")

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self _setupGettingJiggyLabel];
    [self _setupConnectionActivityIndicator];
    
    [self.connectionService connectWithCompletion:^(NSError *error) {

        [self.connectionActivityIndicator stopAnimating];
        
        //transition to feature selection VC
        id featureSelectionVC = [[JSObjection defaultInjector] getObject:@"FeatureSelectionVC"];
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:featureSelectionVC];
        
        MOAppDelegate *appDelegate = (MOAppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate.window setRootViewController:navController];
        
        
        
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark - private

- (void)_setupGettingJiggyLabel {
    
    self.gettingJiggyLabel                  = [[UILabel alloc] initWithFrame:CGRectMake(100, 200, 200, 150)];
    self.gettingJiggyLabel.numberOfLines    = 0;
    self.gettingJiggyLabel.font             = [UIFont fontWithName:@"Forte-Line" size:16];
    self.gettingJiggyLabel.textColor        = [UIColor blackColor];
    
    NSString *labelText = @"Getting jiggy\nwith your mokomoko";
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText];
    NSMutableParagraphStyle *paragraphStyle     = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:40];
    [attributedString addAttribute:NSParagraphStyleAttributeName
                             value:paragraphStyle
                             range:NSMakeRange(0, [labelText length])];
    
    self.gettingJiggyLabel.attributedText = attributedString ;
    
    [self.view addSubview:self.gettingJiggyLabel];
}

- (void)_setupConnectionActivityIndicator {
    
    self.connectionActivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.connectionActivityIndicator.center = CGPointMake(self.view.center.x, self.view.center.y + 100);
    [self.view addSubview:self.connectionActivityIndicator];
    [self.connectionActivityIndicator startAnimating];
}

@end

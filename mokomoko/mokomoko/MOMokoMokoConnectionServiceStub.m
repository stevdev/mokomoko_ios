//
//  MOMokoMokoConnectionServiceStub.m
//  mokomoko
//
//  Created by steffen on 07/04/14.
//  Copyright (c) 2014 mokomoko. All rights reserved.
//

#import "MOMokoMokoConnectionServiceStub.h"

@implementation MOMokoMokoConnectionServiceStub

- (void)connectWithCompletion:(void (^)(NSError *))completion {
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        completion(nil);
    });
}

- (void)sendMatrix:(NSArray*)matrix {
    
    NSLog(@"zzzzZZZZZZZ .... Doing nothing, dude!");
}

@end

//
//  MOFeatureSelectionViewController.m
//  mokomoko
//
//  Created by steffen on 02/04/14.
//  Copyright (c) 2014 mokomoko. All rights reserved.
//

#import "MOFeatureSelectionViewController.h"

@interface MOFeatureSelectionViewController ()

@property (strong, nonatomic) IBOutlet UIButton *letsPartyBtn;
@property (strong, nonatomic) IBOutlet UIButton *letsChillButton;
@property (strong, nonatomic) IBOutlet UIButton *letsPlayButton;

@end

@implementation MOFeatureSelectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.letsPartyBtn.titleLabel.font       = [UIFont fontWithName:@"Forte-Line" size:24];
    self.letsPartyBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    self.letsChillButton.titleLabel.font    = [UIFont fontWithName:@"Forte-Line" size:24];
    self.letsChillButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    self.letsPlayButton.titleLabel.font     = [UIFont fontWithName:@"Forte-Line" size:24];
    self.letsPlayButton.titleLabel.adjustsFontSizeToFitWidth = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

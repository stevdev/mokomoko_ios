//
//  MOPlayViewController.h
//  mokomoko
//
//  Created by steffen on 22/04/14.
//  Copyright (c) 2014 mokomoko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MOPlayViewController : UICollectionViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@end

//
//  MOMatrixController.m
//  mokomoko
//
//  Created by steffen on 23/04/14.
//  Copyright (c) 2014 mokomoko. All rights reserved.
//

#import "MOMatrixController.h"

@implementation MOMatrixController

- (NSUInteger) nrOfRows {
    return 7;
}

- (NSUInteger) nrOfColumns {
    return 7;
}

- (NSIndexPath *) matrixIndexFromPixelWithNr:(NSUInteger)pixelNr {
    
    NSUInteger row    = pixelNr / [self nrOfColumns];
    NSUInteger column = pixelNr % [self nrOfColumns];
    
    NSUInteger indexes[] = {row, column};
    return [NSIndexPath indexPathWithIndexes:indexes length:2];
}

@end

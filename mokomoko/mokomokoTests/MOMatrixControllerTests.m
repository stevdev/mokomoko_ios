//
//  MOMatrixControllerTests.m
//  mokomoko
//
//  Created by steffen on 23/04/14.
//  Copyright (c) 2014 mokomoko. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "P_MOMatrixController.h"
#import "MOMatrixController.h"

@interface MOMatrixControllerTests : XCTestCase

@end

@implementation MOMatrixControllerTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)test_nrOfRows_and_nrOfColumns {
    
    id<P_MOMatrixController> controller = [[MOMatrixController alloc] init];
    
    XCTAssertEqual([controller nrOfRows], 7, @"wrong number of rows in matrix");
    XCTAssertEqual([controller nrOfColumns], 7, @"wrong number of columns in matrix");
}

- (void)test_matrixIndexFromPixelWithNr {
    
    id<P_MOMatrixController> controller = [[MOMatrixController alloc] init];
    
    NSIndexPath *actualIndexPath = [controller matrixIndexFromPixelWithNr:13];
    
    XCTAssertEqual(actualIndexPath.length, 2, @"index path must have two indices");
    
    NSUInteger indexes[] = {0, 0};
    [actualIndexPath getIndexes:indexes];
    
    XCTAssertEqual(indexes[0], 1, @"wrong first index in %d x %d matrix", [controller nrOfRows], [controller nrOfColumns]);
    XCTAssertEqual(indexes[1], 6, @"wrong second index in %d x %d matrix", [controller nrOfRows], [controller nrOfColumns]);

}

@end
